import 'package:flutter/material.dart';

//Color that we use in our app
const KPrimaryColor = Color(0xFF693a70);
const KTextColor = Colors.black;
const KBackgroundColor = Color(0xFFF9F8FD);
const KPrimaryLightColor = Color(0xFFe2bfe8);

const double KDefaultPadding = 20.0;

const kSpacingUnit = 10;

final kTitleTextStyle = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w600,
);

BoxDecoration avatarDecoration =
    BoxDecoration(shape: BoxShape.circle, color: KPrimaryColor, boxShadow: [
  BoxShadow(
    color: Colors.white,
    offset: Offset(10, 10),
    blurRadius: 10,
  ),
  BoxShadow(
    color: Colors.white,
    offset: Offset(-10, -10),
    blurRadius: 10,
  ),
]);
