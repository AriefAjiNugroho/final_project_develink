import 'package:finalsanbercodeproject/constants.dart';
import 'package:flutter/material.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        child: child,
        margin: EdgeInsets.symmetric(vertical: 5),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        width: size.width * 0.8,
        decoration: BoxDecoration(
          color: KPrimaryLightColor,
          borderRadius: BorderRadius.circular(29),
        ));
  }
}
