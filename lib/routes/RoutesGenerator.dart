// import 'package:finalsanbercodeproject/Screens/HomeScreen.dart';
// import 'package:finalsanbercodeproject/Screens/LoginScreen.dart';
// import 'package:flutter/material.dart';

// class RouteGenerator {
//   static Route<dynamic> generateRoute(RouteSettings settings) {
//     // jika ingin mengirim argument
//     // final args = settings.arguments;
//     switch (settings.name) {
//       case '/':
//         return MaterialPageRoute(builder: (_) => LoginScreen());
//       case '/menuUtama':
//         return MaterialPageRoute(builder: (_) => HomeScreen());
//       // return MaterialPageRoute(builder: (_) => AboutPage(args));
//       default:
//         return _errorRoute();
//     }
//   }

//   static Route<dynamic> _errorRoute() {
//     return MaterialPageRoute(builder: (_) {
//       return Scaffold(
//         appBar: AppBar(title: Text("Error")),
//         body: Center(child: Text('Error page')),
//       );
//     });
//   }
// }
