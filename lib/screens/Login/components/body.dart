import 'package:finalsanbercodeproject/components/already_have_an_account.dart';
import 'package:finalsanbercodeproject/components/rounded_button.dart';
import 'package:finalsanbercodeproject/components/rounded_input_field.dart';
import 'package:finalsanbercodeproject/components/rounded_password_field.dart';
import 'package:finalsanbercodeproject/constants.dart';
import 'package:finalsanbercodeproject/screens/Home/home.dart';
import 'package:finalsanbercodeproject/screens/Login/components/background.dart';
import 'package:finalsanbercodeproject/screens/Signup/signup_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  // const Body({Key? key}) : super(key: key);

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Login",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: KPrimaryColor,
                fontSize: 30),
          ),
          SizedBox(height: size.height * 0.03),
          SvgPicture.asset(
            "assets/icons/Secure login-amico.svg",
            height: size.height * 0.3,
          ),
          SizedBox(height: size.height * 0.03),
          RoundedInputField(
            hintText: "Your Email",
            textEditingController: _emailController,
            onChanged: (value) {},
          ),
          RoundedPasswordField(
            hintText: "Password",
            textEditingController: _passwordController,
            onChanged: (value) {},
          ),
          RoundedButton(
              text: "Login",
              press: () async {
                try {
                  await _firebaseAuth
                      .signInWithEmailAndPassword(
                        email: _emailController.text,
                        password: _passwordController.text,
                      )
                      .then((value) => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (context) => HomeScreen())));
                } catch (e) {
                  // ignore: non_constant_identifier_names
                  String TextOuput = '';
                  if (e.toString() ==
                      '[firebase_auth/unknown] Given String is empty or null') {
                    TextOuput = 'Isi email dan password';
                  } else if (e.toString() ==
                      '[firebase_auth/invalid-email] The email address is badly formatted.') {
                    TextOuput = 'format email salah';
                  } else if (e.toString() ==
                      '[firebase_auth/user-not-found] There is no user record corresponding to this identifier. The user may have been deleted.') {
                    TextOuput = 'email tidak terdaftar atau sudah dihapus';
                  } else {
                    print("salah gegara ${e.toString()}");
                  }
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(TextOuput)));
                }
              }),
          SizedBox(height: size.height * 0.03),
          AlredyHaveAnAccountCheck(
            login: true,
            press: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return SignUpScreen();
                },
              ));
            },
          )
        ],
      ),
    );
  }
}
