import 'package:finalsanbercodeproject/components/drawers_screen.dart';
import 'package:finalsanbercodeproject/constants.dart';
import 'package:finalsanbercodeproject/screens/Home/HomeBar/home_screen.dart';
import 'package:finalsanbercodeproject/screens/Home/NewsBar/news_screen.dart';
import 'package:finalsanbercodeproject/screens/Home/ProfilBar/profil_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentTab = 0;
  final List<Widget> screens = [HomeBar(), NewsBar(), ProfilBar()];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = HomeBar();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
      drawer: DrawerScreen(),
      body: PageStorage(bucket: bucket, child: currentScreen),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen = HomeBar();
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: currentTab == 0
                              ? KPrimaryColor
                              : KPrimaryLightColor,
                        ),
                        Text(
                          'Home',
                          style: TextStyle(
                              color: currentTab == 0
                                  ? KPrimaryColor
                                  : KPrimaryLightColor),
                        )
                      ],
                    ),
                    minWidth: 40,
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen = NewsBar();
                        currentTab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.info,
                          color: currentTab == 1
                              ? KPrimaryColor
                              : KPrimaryLightColor,
                        ),
                        Text(
                          'News',
                          style: TextStyle(
                              color: currentTab == 1
                                  ? KPrimaryColor
                                  : KPrimaryLightColor),
                        )
                      ],
                    ),
                    minWidth: 40,
                  ),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        currentScreen = ProfilBar();
                        currentTab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.account_circle,
                          color: currentTab == 2
                              ? KPrimaryColor
                              : KPrimaryLightColor,
                        ),
                        Text(
                          'Profil',
                          style: TextStyle(
                              color: currentTab == 2
                                  ? KPrimaryColor
                                  : KPrimaryLightColor),
                        )
                      ],
                    ),
                    minWidth: 40,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
