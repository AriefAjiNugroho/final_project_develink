import 'package:finalsanbercodeproject/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser!.email);
    }
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 42.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 16),
                Text.rich(
                  TextSpan(
                      text: 'Welcome, ',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: KPrimaryLightColor),
                      children: [
                        TextSpan(
                          text: auth.currentUser!.email,
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.normal,
                              color: KPrimaryColor),
                        )
                      ]),
                  style: TextStyle(fontSize: 20),
                ),
                SvgPicture.asset(
                  "assets/icons/Home.svg",
                  height: size.height * 0.45,
                ),
                Text(
                  'Football News',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, color: KPrimaryColor),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'UNITED CONFIRM RONALDO AGREEMENT',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.black),
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset("assets/images/RonaldoBack.jpg"),
                SizedBox(
                  height: 10,
                ),
                Text.rich(TextSpan(
                  style: TextStyle(),
                  text:
                      "Manchester United is delighted to confirm that the club has reached agreement with Juventus for the transfer of Cristiano Ronaldo, subject to agreement of personal terms, visa and medical. Cristiano, a five-time Ballon d’Or winner, has so far won over 30 major trophies during his career, including five UEFA Champions League titles, four FIFA Club World Cups, seven league titles in England, Spain and Italy, and the European Championship for his native Portugal. In his first spell for Manchester United, he scored 118 goals in 292 games. Everyone at the club looks forward to welcoming Cristiano back to Manchester.",
                )),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'https://www.manutd.com/en/news/detail/official-statement-on-agreement-reached-for-cristiano-ronaldo',
                  textAlign: TextAlign.end,
                  style: TextStyle(color: KPrimaryColor),
                ),
                SizedBox(
                  height: 40,
                ),
                Text(
                  'Watch: De Jong and Fati impress in Barcelona training ahead of Bayern Munich clash',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, color: Colors.black),
                ),
                SizedBox(
                  height: 10,
                ),
                Image.asset("assets/images/ansu_fati.jpeg"),
                SizedBox(
                  height: 10,
                ),
                Text.rich(TextSpan(
                  style: TextStyle(),
                  text:
                      "With their clash against Sevilla suspended, Barca continued training on Saturday as they prepared for their opening Champions League game against Bayern Munich. Rondo, crossing and shooting drills were all held, with new signing Luuk de Jong showing form in front of goal and Ansu Fati also catching the eye as he works his way to full fitness.",
                )),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'https://www.tribalfootball.com/articles/watch-de-jong-and-fati-impress-in-barcelona-training-ahead-of-bayern-munich-clash-4385598',
                  textAlign: TextAlign.end,
                  style: TextStyle(color: KPrimaryColor),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
