import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(MaterialApp(
    home: NewsBar(),
  ));
}

class NewsBar extends StatelessWidget {
  final String apiUrl =
      "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=2ea07f7a019846c4bb8387af9e44022c";
  Future<List<dynamic>> _fecthDataUsers() async {
    var result = await http.get(Uri.parse(apiUrl));
    return json.decode(result.body)['articles'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder<List<dynamic>>(
          future: _fecthDataUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  padding: EdgeInsets.all(10),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () {},
                        child: Container(
                          width: 48,
                          height: 48,
                          padding: const EdgeInsets.symmetric(vertical: 4.0),
                          alignment: Alignment.center,
                          child: CircleAvatar(
                              radius: 30,
                              backgroundImage: snapshot.data[index]
                                          ['urlToImage'] !=
                                      null
                                  ? NetworkImage(
                                      snapshot.data[index]['urlToImage'])
                                  : NetworkImage(
                                      'https://i.kinja-img.com/gawker-media/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/a45d96107ed1ff2c64e55e3625f585c9.jpg')),
                        ),
                      ),
                      title: snapshot.data[index]['title'] != null
                          ? Text(snapshot.data[index]['title'])
                          : Text("-"),
                      dense: false,
                      contentPadding: EdgeInsets.all(5),
                      subtitle: snapshot.data[index]['content'] != null
                          ? Text(snapshot.data[index]['content'])
                          : Text('-'),
                    );
                  });
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}
