import 'package:finalsanbercodeproject/components/already_have_an_account.dart';
import 'package:finalsanbercodeproject/components/rounded_button.dart';
import 'package:finalsanbercodeproject/components/rounded_input_field.dart';
import 'package:finalsanbercodeproject/components/rounded_password_field.dart';
import 'package:finalsanbercodeproject/constants.dart';
import 'package:finalsanbercodeproject/screens/Home/home.dart';
import 'package:finalsanbercodeproject/screens/Login/login_screen.dart';
import 'package:finalsanbercodeproject/screens/Signup/components/background.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Sign Up",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: KPrimaryColor,
                fontSize: 30),
          ),
          SizedBox(height: size.height * 0.03),
          SvgPicture.asset(
            "assets/icons/SignUp.svg",
            height: size.height * 0.3,
          ),
          SizedBox(height: size.height * 0.03),
          RoundedInputField(
            hintText: "Your Email",
            textEditingController: _emailController,
            onChanged: (value) {},
          ),
          RoundedPasswordField(
            hintText: "Password",
            textEditingController: _passwordController,
            onChanged: (value) {},
          ),
          RoundedButton(
              text: "Sign up",
              press: () async {
                try {
                  await _firebaseAuth
                      .createUserWithEmailAndPassword(
                        email: _emailController.text,
                        password: _passwordController.text,
                      )
                      .then((value) => Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                              builder: (context) => HomeScreen())));
                } catch (e) {
                  // ignore: non_constant_identifier_names
                  String TextOuput = '';
                  if (e.toString() ==
                      '[firebase_auth/unknown] Given String is empty or null') {
                    TextOuput = 'Isi email dan password';
                  } else if (e.toString() ==
                      '[firebase_auth/invalid-email] The email address is badly formatted.') {
                    TextOuput = 'format email salah';
                  } else if (e.toString() ==
                      '[firebase_auth/weak-password] Password should be at least 6 characters') {
                    TextOuput = 'password minimal 6 karakter';
                  } else if (e.toString() ==
                      '[firebase_auth/email-already-in-use] The email address is already in use by another account.') {
                    TextOuput = 'email sudah terpakai';
                  } else {
                    print("salah gegara ${e.toString()}");
                  }
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(TextOuput)));
                }
              }),
          SizedBox(height: size.height * 0.03),
          AlredyHaveAnAccountCheck(
            login: false,
            press: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return LoginScreen();
                },
              ));
            },
          ),
          // OrDivider(), // --------------OR----------------
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [
          //     SocialIcon(
          //       iconSrc: "assets/icons/Secure login-amico.svg",
          //       press: () {},
          //     ),
          //     SocialIcon(
          //       iconSrc: "assets/icons/Secure login-amico.svg",
          //       press: () {},
          //     ),
          //     SocialIcon(
          //       iconSrc: "assets/icons/Secure login-amico.svg",
          //       press: () {},
          //     ),
          //   ],
          // )
        ],
      ),
    );
  }
}

class SocialIcon extends StatelessWidget {
  final String iconSrc;
  final Function() press;
  const SocialIcon({
    Key? key,
    required this.iconSrc,
    required this.press,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          border: Border.all(
            width: 2,
            color: KPrimaryLightColor,
          ),
          shape: BoxShape.circle,
        ),
        child: SvgPicture.asset(
          iconSrc,
          height: 20,
          width: 20,
        ),
      ),
    );
  }
}
