import 'package:finalsanbercodeproject/components/rounded_button.dart';
import 'package:finalsanbercodeproject/constants.dart';
import 'package:finalsanbercodeproject/screens/Login/login_screen.dart';
import 'package:finalsanbercodeproject/screens/Signup/signup_screen.dart';
import 'package:finalsanbercodeproject/screens/Welcome/components/background.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size privide  us total height and width of our screen
    return Background(
        child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            children: [
              Text(
                "DEVELINK",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: KPrimaryColor,
                    fontSize: 30),
              ),
            ],
          ),
          SizedBox(height: size.height * 0.03),
          SvgPicture.asset(
            "assets/icons/welcome.svg",
            height: size.height * 0.45,
          ),
          SizedBox(height: size.height * 0.03),
          RoundedButton(
            text: "Login",
            press: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return LoginScreen();
                },
              ));
            },
          ),
          OrDivider(),
          RoundedButton(
            text: "Daftar",
            color: KPrimaryLightColor,
            textColor: Colors.black,
            press: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return SignUpScreen();
                },
              ));
            },
          )
        ],
      ),
    ));
  }
}

class OrDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: size.height * 0.02),
      width: size.width * 0.8,
      child: Row(
        children: [
          buildDivider(), //menambah garis
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              "atau",
              style: TextStyle(
                color: KPrimaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          buildDivider(), //menambah garis
        ],
      ),
    );
  }
}

Expanded buildDivider() {
  return Expanded(
    child: Divider(
      color: Color(0xFFD9D9D9),
      height: 1.5,
    ),
  );
}
